package it.pkg.config.dozer;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DozerConfig {

    @Bean
    @Primary
    Mapper beanMapper() {
        Mapper beanMapper = DozerBeanMapperBuilder.buildDefault();
        return beanMapper;
    }
}