package it.pkg.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dozermapper.core.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public abstract class AbstractController {
    @Autowired
    protected Mapper beanMapper;

    @Autowired
    protected ObjectMapper jsonMapper;
}
