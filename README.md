# Maven Archetype

Archetype to generate a project with boilerplate code for a typical spring-based web app. Features following pre-configured modules: DozerMapper, Jackson, JPA and an abstract Controller.

## Generate new project in interactive mode

mvn archetype:generate \
    -DarchetypeGroupId=eth.locko \
    -DarchetypeArtifactId=spring-boot-3-web-jpa-archetype
